#!/usr/bin/env bash

{ # try

    latexmk -bibtex -f -interaction=nonstopmode -pdf -pdflatex="pdflatex --shell-escape %O %S" report

} || { # catch

    echo "report compiled with errors"
    
}