\documentclass[]{IEEEtran}
% If IEEEtran.cls has not been installed into the LaTeX system files,
% manually specify the path to it like:
% \documentclass[conference]{../sty/IEEEtran}

% Your packages go here
\usepackage[utf8]{inputenc}
\usepackage{mathtools}
\usepackage{blkarray} %
\usepackage{listings} %Package to include

\markboth{T-578-ITCV Computer Vision}{}

\begin{document}

  \title{Project 3}

  \author{
    \IEEEauthorblockN{Casimir Wallwitz, Leith Hobson and Stefán Laxdal}

    \thanks{casimir21, leith21, stefanl20}{casimir21, leith21, stefanl20}
  }

  \maketitle


  \begin{abstract}
    In this paper, we describe our process of training a convolutional neural network (CNN) to determine whether or not a face in a given picture is wearing a mask. By using a pre-trained face detector to focus only on the area of interest, we managed to build a network that performed well in average conditions but could be broken relatively easily, especially when intending to fool the network.

  \end{abstract}

  \section{Introduction}
  \label{sec:introduction}
    Project 3 was aimed at creating a mask detector that could identify faces and categorise them into two subgroups, faces with masks and faces without. Early discussions introduced the idea of using some sort of blob detector to identify a large mass where a mouth should be, or simply to use template matching to detect the absence of a mouth. Another idea was to utilise colour matching to identify medical masks. We however decided that a convolutional neural network was likely to produce the best results. We examined many existing approaches to similar problems and concluded that we would design our network in such a way that it reported through two output neurons, one for the probability of wearing a mask and one for the probability of not wearing one. For our framework we chose Pytorch as it allows for granular control of the network design while abstracting away much of the mathematical aspects we were likely to get wrong, as well as GPU compute power, when available. We did consider using transfer learning to extend an already pre-trained model to serve our application, as suggested by our readings, but chose instead to build our own model from scratch.

  
  
    \section{Related work}
    As the pandemic has now gone on for nearly two years at writing, there are many implementations of this problem on the internet, which gave us a great starting point.
    Still, we tried not to follow tutorials such as \cite{TFtutorial} or \cite{gurukul} too closely and do some things on our own. One example of that would be the use of our own simple Network architecture instead of using a pre-existing one such as MobileNetV2 as in \cite{TFtutorial} or YOLO or even a pretrained model, which we did consider at first. This turned out to be a great decision, as it gave us much more flexibility in our experiments and made the training of our model much quicker than it would have been with a larger network.\\
    However, we did struggle with similar issues as \cite{TFtutorial}: We have no control over the quality of the face detector \cite{FaceDetector}, and chose not to focus our attention on creating an improved one, so as mentioned in that article, we might not detect a face that is too obscured by a mask or otherwise somehow different from what that specific face detector might expect. This has not proven to be a very serious problem in practice for us, so we did not experiment too much with the face detection. \\
    Apart from that, both \cite{TFtutorial} and \cite{gurukul} managed to achieve accuracies close to 99\%, so while it might not appear as accurate in practice when testing the edge cases of the model, on images with average conditions it is expected to perform very well.
    \\
    We used two datasets, specifically from \cite{humansInLoop} and  \cite{cabani.hammoudi.2020.maskedfacenet}\cite{cmes.2020.011663}, while most other implementations seem to either create their own ones similar to the later dataset. We expected to face some problems connected to the artificiality of the dataset, and the lack of variety in masks, however we could not connect any of the errors we had to that with certainty, especially as the network was able to identify a variety of masks, regardless of style.
    \section{Methodology}
    \subsection*{Model}
    The model was built using Pytorch and is very simple. It consists of three convolutional layers, each followed by a max-pooling and a rectified linear unit activation function as non-linearity. Finally there is one fully connected layer that leads to the two output neurons, which are returned using a softmax function to normalise the output sum. 
    \\
    The first convolutional layer receives only one channel, doing so because the input images have all been converted to greyscale, and outputs 32 channels. Every subsequent convolutional layer then doubles the amount of channels. \\
    The final fully connected layer receives 512 channels and compresses them into two output neurons.
\begin{figure}[h]
        \centering
        \includegraphics[scale=0.2]{media/nn.png}
        \caption{Visualization of the network architecture}
        \label{fig:my_label}
    \end{figure}
    \newpage
    \subsection*{Data Generation}
    Before each training cycle the option of generating the training data, or utilising the existing training data set is presented. Generation of the dataset is done by looping over every available training image, cropping where necessary, and processing the images. The processing involves scaling the images down to be 50x50 in size and converting them to grey scale. The image is then converted to a numpy array and has its corresponding label attached, this together is in turn placed in a larger numpy array. Once all the images have been processed in this way they are shuffled in place and stored for future use.
    \begin{lstlisting}[language=Python]
for label in self.LABELS:
  for f in os.listdir(label):

    path = os.path.join(label, f)
    img = cv2.imread(path,
        cv2.IMREAD_greySCALE)

    img = cv2.resize(img,(50, 50))
    self.training_data.append(
        [np.array(img), 
        np.eye(2)[self.LABELS[label]]]) 

    if label == NO_MASK_DIR:
      self.noMaskCount += 1
    elif label == MASK_DIR:
      self.maskCount += 1


    np.random.shuffle(self.training_data)
    np.save(TRAINING_DATA_PATH,
        self.training_data)

    \end{lstlisting}
    \subsection*{Training}
The actual training begins by loading the training data, converting it to Pytorch tensors and normalising it. The data is then partitioned into a training dataset and a testing dataset. Usually ten percent of the total data is used for the testing dataset, although this can be configured through the constants. This is also where our optimizer and loss function is selected. In our case we used the Adam optimizer and a cross entropy loss function.
\\
\begin{lstlisting}[language=Python]
optimizer = optim.Adam(net.parameters(),
    lr=0.001)
loss_function = nn.CrossEntropyLoss()

features = torch.Tensor(
    [i[0] for i in training_data])
    .view(-1, 50, 50)
features = features/255.0
labels = torch.Tensor(
    [i[1] for i in training_data])

test_size = int(len(features)*TEST_PERCENT)

train_features = features[:-test_size]
train_labels = labels[:-test_size]

test_features = features[-test_size:]
test_labels = labels[-test_size:]
\end{lstlisting}

Once all initialisation is finished we begin looping through our predefined epochs. Every epoch our data is put through the network using the forward function defined on the model. This is done in batches to conserve memory, in our case, we have experimented with batch sizes in the range 100 - 200. For every batch the network's predictions are gathered and the loss function used to calculate accuracy. Pytorch computes each neuron's gradient function as it processes the forward function, so performing back propagation is trivial. \\
For every epoch we then calculate the accuracy of the model in its current state. If the model has achieved a better level of accuracy than previously seen, its state is automatically saved and kept through the coming epochs. This ensures that if accuracy starts declining because of over training, the best identified model is still stored at the end of our program.
\\
\begin{lstlisting}[language=Python]
for epoch in range(EPOCHS):
  accuracy = 0
  num_of_steps = 0
  for i in range(0, 
        len(train_features),
        BATCH_SIZE):
    batch_features = train_features[
        i:i + BATCH_SIZE]
            .view(-1, 1, 50, 50)
    batch_labels = train_labels[
        i:i+BATCH_SIZE]

    optimizer.zero_grad()
    outputs = net(batch_features)

    loss = loss_function(outputs, batch_labels)
    loss.backward()
    optimizer.step()
\end{lstlisting}

\subsection*{Player}
The player begins by reading the video feed coming from a camera connected to the computer. Every frame is first checked for faces, using a pretrained neural network that provides the location of each face it finds in the frame. If at least one face is found in the frame all identified faces are cropped to the coordinates given by the face detector network, and those faces sent to the mask detector. Our network will then return a prediction of mask wearing probability for every face and those predictions displayed on the screen along with a colour coded bounding box for the predicted face. 
\newpage
    \section{Experiements}
The first experiments were run using a small dataset, of which we could use 900 masked images and 900 unmasked images, due to data balancing limiting breadingroth group sizes to the smaller group size. The training was done on both 10 and 20 epochs and would generally result in an 80 - 87 percent accuracy at the 10-20 epoch range. We experimented with changing layers and dimensions, removing and adding layers and removing and changing max-pooling functions. In every such case our accuracy would drop dramatically, to between 50 - 70 percent accuracy. We therefore decided to stick to our original model and try to train it on a larger dataset.

\begin{figure}[h]
        \centering
        \includegraphics[scale=0.5]{media/norotate.png}
        \caption{Sample run of the smaller dataset.}
        \label{fig:my_label}
    \end{figure}
    
We then added a much larger dataset to our pool of images, bringing our total images up to around 130.000. Using all of those images we quickly discovered that the accuracy of the model would be around 97 percent after the first and second epoch, then sowly rising to 99 percent for the remainder of the epochs. We created fully functional models after two epochs and tried running 50 epochs to see if the accuracy would drop, which it did not. We concluded that the network had found some local minimum and that a more complex network is required to take advantage of such a large amount of data.

\begin{figure}[h]
        \centering
        \includegraphics[scale=0.5]{media/latest-train.png}
        \caption{Latest train run of the final network. Shows fast convergence and less accuracy fluctuation.}
        \label{fig:my_label}
    \end{figure}

\section{Results}
Though our model performed better than expected, we did encounter some problems with its real world accuracy. \\
The detector works as intended when a person is looking directly into the camera from any angle in front of the persons face. It mostly works when a face is looked at from a side angle, but becomes worse with the increase of that angle and stops working when at a 90 degree angle. \\
One large problem becomes evident when a person rotates their head to one side. When wearing a mask it mostly works, but with a lesser confidence, while when not wearing a mask the detector will often incorrectly detect a mask on a tilted head. We attempted to mitigate this by artificially rotating some of the images we used when generating the test data, but this did not solve the problem completely. 
\\
Another point worth mentioning is that we did not define any class to capture a face wearing a mask incorrectly. We bundled some images of incorrectly worn masks into our no mask test and training set. Because of this the model has to make a binary prediction between a mask and no mask, and sometimes behaves erratically when dealing with edge cases such as a mask placed only over the mouth. 
Further, the lack of counter examples in the data, such as of people holding their hands, or cellphone in front of their faces as masks lead the network to sometimes incorrectly identify these facial obscurities as masks.
\\ 


  % \newpage
  \section{Conclusion}
  \label{sec:conclusion}

    We have have discussed the process through which we built and trained a convolutional neural network to determine whether or not a face in a given picture is wearing a mask. We have been pleasantly surprised by our own results, however, with further time, and additional data sets we would like to further test and optimise our model, in order to determine which accuracy levels we are able to achieve.

  \section{Contribution}
  \label{sec:contribution}

    \begin{equation*}
      \begin{blockarray}{*{5}{c} l}
        \begin{block}{*{5}{>{$\footnotesize}c<{$}} l}
          Preprocessing & Network Architecture & Report \\
        \end{block}
        \begin{block}{[*{5}{c}]>{$\footnotesize}l<{$}}
          35 & 32 & 33 & Casimir \\
          35 & 33 & 33 & Leith \\
          30 & 35 & 34 & Stefán \\
        \end{block}
      \end{blockarray}
    \end{equation*}

\newpage
\bibliographystyle{plain}
\bibliography{bib}

\end{document}