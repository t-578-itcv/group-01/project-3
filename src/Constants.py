import sys

BATCH_SIZE = 150  # Change for memory errors
EPOCHS = 20

TRAIN_RATIO = 0.08
TEST_PERCENT = TRAIN_RATIO

REBUILD_DATA = True

PATH = sys.path[0] + "/../"
ANN_PATH = PATH + "input/Medical mask/Medical Mask/annotations"
IMG_PATH = PATH + "input/Medical mask/Medical Mask/images"

MASK_DIR = PATH + "input/images/face_with_mask/"
NO_MASK_DIR = PATH + "input/images/face_no_mask/"
MASK_INCR_DIR = PATH + "input/images/face_with_mask_incorrect/"

TRAINING_DATA_PATH = PATH + "output/training_data.npy"
MODEL_PATH = PATH + "output/LaxNet.pt"
PLOT_PATH = PATH + "output/plot.png"

CLEAN_IMAGES_PATH = PATH + "input/images/"


TAGS_OF_INTEREST = ['face_no_mask', 'face_with_mask_incorrect',
                    'face_with_mask', 'face_other_covering']

# TAGS_OF_INTEREST = ['mask_colorful', 'face_no_mask', 'face_with_mask']


SPLIT_TRAIN_TEST = False

KEY_Q = 113
KEY_ESC = 27
