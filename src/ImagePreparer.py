#!/bin/python3

import cv2 as cv
import json
import os
import sys
from collections import defaultdict

from Constants import ANN_PATH, IMG_PATH, TAGS_OF_INTEREST, CLEAN_IMAGES_PATH, TRAIN_RATIO, SPLIT_TRAIN_TEST


class ImagePreparer:

    def downloadImages(self):
        # This is handled in the loadData bash script which runs before this
        pass

    def returnZero(self):
        return 0

    def sortImages(self):
        status = ""
        matches = 0
        imageCount = 0
        self.testCount = defaultdict(self.returnZero)
        self.trainCount = defaultdict(self.returnZero)
        print("Beginning walk..")
        failedImages = []
        fileList = os.listdir(ANN_PATH)
        fileListLength = fileList.__len__()
        for filename in fileList:

            with open(ANN_PATH+"/"+filename) as f:
                imageCount += 1
                j = json.load(f)
                filename = j["FileName"]
                annotations = j["Annotations"]

                matchesOnImage = 0

                img = cv.imread(IMG_PATH+"/"+filename)
                for an in annotations:
                    class_name = an["classname"]
                    if class_name in TAGS_OF_INTEREST:
                        try:
                            bBox = an["BoundingBox"]
                            x, y, n, m = bBox
                            croppedImg = img[y:m, x:n]
                            testOrTrain = "train/"
                            if(not SPLIT_TRAIN_TEST):
                                testOrTrain = ''
                                self.trainCount[class_name] += 1
                            elif(SPLIT_TRAIN_TEST and (self.trainCount[class_name] * TRAIN_RATIO > self.testCount[class_name])):
                                testOrTrain = "test/"
                                self.testCount[class_name] += 1
                            else:
                                self.trainCount[class_name] += 1

                            ext = os.path.splitext(filename)

                            outputImage = f"{CLEAN_IMAGES_PATH}{testOrTrain}{class_name}/{ext[0]}_{str(matchesOnImage)}{ext[1]}"
                            cv.imwrite(
                                outputImage, croppedImg)

                            matchesOnImage += 1
                            matches += 1

                            status = (str(matches)+" matches from " + str(imageCount) + " of " + str(
                                fileListLength) + " images with " + str(failedImages.__len__()) + " errors")
                            self.printStatus(status)

                        except:
                            print(
                                "                Error processing image: " + str(filename) + "           ")
                            failedImages.append(filename)
                            # sys.exit(1)
        print(status)
        print(str(matches) +
              " matches found.\n" + str(failedImages.__len__()) + " images failed")
        for image in failedImages:
            print(image)

    def balanceGroups(self):
        # This is not full proof, but in the scope, we can safely assume that we will not have a larger training group than this
        smallestGroup = 9999999999
        smallestGroupName = ''
        for keyword in TAGS_OF_INTEREST:
            if(self.trainCount[keyword] < smallestGroup):
                smallestGroup = self.trainCount[keyword]
                smallestGroupName = keyword

        print("Smallest group:", smallestGroupName, "with", smallestGroup)

    def create_directories(self):
        self.createDirectory(CLEAN_IMAGES_PATH)
        if(SPLIT_TRAIN_TEST):

            for testOrTrain in ['test/', 'train/']:
                self.createDirectory(CLEAN_IMAGES_PATH + testOrTrain)
                for keyword in TAGS_OF_INTEREST:
                    self.createDirectory(
                        CLEAN_IMAGES_PATH + testOrTrain + keyword)

        else:
            for keyword in TAGS_OF_INTEREST:
                self.createDirectory(
                    CLEAN_IMAGES_PATH + keyword)

    def printStatus(self, status):

        print(status, end="\r")

    def createDirectory(self, directory):
        try:
            os.mkdir(directory)
        except:
            print("Failed to create folder: ", directory)
            # sys.exit(1)

    def main(self):
        self.downloadImages()
        print("Creating directories")
        self.create_directories()
        print("Sorting images")
        self.sortImages()
        print("Balancing groups")
        self.balanceGroups()


if __name__ == "__main__":
    ImagePreparer().main()
