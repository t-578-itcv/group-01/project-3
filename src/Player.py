import torch
import sys
import numpy as np
import imutils
import cv2
from imutils.video import VideoStream
from Net import Net
from tqdm import tqdm
import torchvision.transforms as transforms

from Constants import PATH, MODEL_PATH, KEY_ESC, KEY_Q


class Player:

    protoxtPath = PATH + "/face_detector/deploy.prototxt"
    weightsPath = PATH + "/face_detector/res10_300x300_ssd_iter_140000.caffemodel"

    def __init__(self, features=None, labels=None):
        self.features = features
        self.labels = labels
        self.net = Net()
        self.vs = VideoStream(src=0)
        self.net.load_state_dict(torch.load(MODEL_PATH))
        self.net.eval()
        self.transform = transforms.ToTensor()
        self.faceNet = cv2.dnn.readNet(self.protoxtPath, self.weightsPath)

    def predict_mask(self, faces):
        preds = []
        for face in faces:
            net_out = self.net(face.view(-1, 1, 50, 50))[0]
            preds.append((net_out[0], net_out[1]))
        return preds

    def preprocess_face(self, frame, DISPLAY_FACE=True):
        face = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        if DISPLAY_FACE:
            face = cv2.resize(face, (50, 50))
        cv2.imshow("face", face)
        face = self.transform(face)
        return face

    def find_faces(self, frame):
        faces = []
        locs = []

        h, w = frame.shape[:2]
        blob = cv2.dnn.blobFromImage(
            frame, 1.0, (244, 244), (104.0, 177.0, 123.0))

        self.faceNet.setInput(blob)
        detections = self.faceNet.forward()
        for i in range(0, detections.shape[2]):
            confidence = detections[0, 0, i, 2]

            if confidence > 0.5:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                startX, startY, endX, endY = box.astype("int")

                OFFSET = 20
                startX, startY = (max(0, startX-OFFSET), max(0, startY-OFFSET))
                endX, endY = (min(w - 1, endX+OFFSET), min(h - 1, endY+OFFSET))

                face = self.preprocess_face(frame[startY: endY, startX: endX])
                faces.append(face)
                locs.append((startX, startY, endX, endY))

        preds = None
        if len(faces) > 0:
            preds = self.predict_mask(faces)

        return (locs, preds)

    def predict(self, frame):
        locs, preds = self.find_faces(frame)
        if preds == None:
            return
        for box, pred in zip(locs, preds):
            startX, startY, endX, endY = box
            withoutMask, mask = pred

            label = "Mask" if mask > withoutMask else "No Mask"
            color = (0, 255, 0) if label == "Mask" else (0, 0, 255)

            label = "{}: {:.2f}%".format(label, max(mask, withoutMask) * 100)

            cv2.putText(frame, label, (startX, startY - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.45, color, 2)
            cv2.rectangle(frame, (startX, startY), (endX, endY), color, 2)

    def play(self):
        self.vs.start()

        while True:
            frame = self.vs.read()
            frame = imutils.resize(frame, width=400)

            self.predict(frame)
            cv2.imshow("Frame", frame)

            # ESC or Q to exit
            key = cv2.waitKey(1) & 0xFF
            if key == KEY_ESC or key == KEY_Q:
                break

        cv2.destroyAllWindows()
        self.vs.stop()

#    def play(self):
#        print("Playing..")
#        correct = total = 0
#        print(self.features)
#        print(self.labels)
#        for i in tqdm(range(len(self.features))):
#            real_class = torch.argmax(self.labels[i])
#            net_out = self.net(self.features[i].view(-1, 1, 50, 50))[0]
#            predicted_class = torch.argmax(net_out)
#            if predicted_class == real_class:
#                correct += 1
#            total += 1
#        print("Accuracy: ", round(correct/total, 3))


if __name__ == "__main__":
    P = Player()
    P.play()
