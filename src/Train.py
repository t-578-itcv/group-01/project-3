#!/bin/python3

import cv2
from Player import Player
import os
import sys
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch import flatten
from tqdm import tqdm
import numpy as np
from scipy import ndimage
import random
import matplotlib.pyplot as plt
from Net import Net


from Constants import REBUILD_DATA, TRAINING_DATA_PATH, MODEL_PATH, BATCH_SIZE, EPOCHS, MASK_DIR, NO_MASK_DIR, TEST_PERCENT, PLOT_PATH


class MaskOrNot():
    IMG_SIZE = 50
    LABELS = {NO_MASK_DIR: 0, MASK_DIR: 1}

    training_data = []
    maskCount = 0
    noMaskCount = 0

    def make_training_data(self):
        for label in self.LABELS:
            for f in tqdm(os.listdir(label)):

                if self.noMaskCount < self.maskCount:
                    break

                try:
                    path = os.path.join(label, f)
                    img = cv2.imread(path, cv2.IMREAD_GRAYSCALE)
                    img = cv2.resize(img, (self.IMG_SIZE, self.IMG_SIZE))

                    # DANGER ACHTUNG INGOZI GEVAAR VERSIGTIG
                    angles = [0, 0, 0, 0, 0, 10, -10,
                              20, -20, -30, 30, 50, -50, 70, -70]
                    img = ndimage.rotate(img, random.choice(
                        angles), reshape=False, mode='wrap')

                    self.training_data.append([np.array(img), np.eye(
                        2)[self.LABELS[label]]])  # Create class as vector

                    if label == NO_MASK_DIR:
                        self.noMaskCount += 1
                    elif label == MASK_DIR:
                        self.maskCount += 1

                except Exception as e:
                    print(e)
                    continue

        np.random.shuffle(self.training_data)
        np.save(TRAINING_DATA_PATH, self.training_data)
        print("Mask count: ", self.maskCount)
        print("No mask count: ", self.noMaskCount)


# Determine if a CUDA GPU is available
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
print("Device:", device)

if REBUILD_DATA:
    MaskOrNot().make_training_data()


training_data = np.load(TRAINING_DATA_PATH, allow_pickle=True)

net = Net()

optimizer = optim.Adam(net.parameters(), lr=0.001)
loss_function = nn.CrossEntropyLoss()

features = torch.Tensor([i[0] for i in training_data]).view(-1, 50, 50)
features = features/255.0
labels = torch.Tensor([i[1] for i in training_data])

test_size = int(len(features)*TEST_PERCENT)

train_features = features[:-test_size]
train_labels = labels[:-test_size]

test_features = features[-test_size:]
test_labels = labels[-test_size:]

BEST_MODEL = None
BEST_ACCURACY = 0
accuracies = []
losses = []


def test(net, features, labels):
    with torch.no_grad():
        correct = 0
        total = 0
        print("Testing...")
        for i in tqdm(range(len(features))):
            real_class = torch.argmax(test_labels[i])

            net_out = net(features[i].view(-1, 1, 50, 50).to(device))[0]
            predicted_class = torch.argmax(net_out)
            if predicted_class == real_class:
                correct += 1
            total += 1
        return round(correct/total, 3)


for epoch in range(EPOCHS):
    print("Epoch ", epoch)
    accuracy = 0
    num_of_steps = 0
    for i in tqdm(range(0, len(train_features), BATCH_SIZE)):
        batch_features = train_features[i:i + BATCH_SIZE].view(-1, 1, 50, 50)
        batch_labels = train_labels[i:i+BATCH_SIZE]

        batch_features = batch_features.to(device)
        batch_labels = batch_labels.to(device)

        optimizer.zero_grad()
        outputs = net.to(device)(batch_features)

        loss = loss_function(outputs, batch_labels)
        loss.backward()
        optimizer.step()

    accuracy = test(net, test_features, test_labels)
    _loss = round(loss.item(), 3)
    accuracies.append(accuracy)
    losses.append(_loss)
    if accuracy > BEST_ACCURACY:
        BEST_MODEL = net.state_dict()
        BEST_ACCURACY = accuracy
        print("New best found!")

    print("Accuracy:", accuracy)
    print("Loss:", _loss)


plt.style.use("ggplot")
plt.figure()
plt.plot(np.arange(0, EPOCHS), accuracies, label="Accuracy")
plt.plot(np.arange(0, EPOCHS), losses, label="Loss")
plt.xlabel("Epoch #")
plt.ylabel("Loss/Accuracy")
plt.legend(loc="lower left")
plt.title("Training")
plt.savefig(PLOT_PATH)


torch.save(BEST_MODEL, MODEL_PATH)
