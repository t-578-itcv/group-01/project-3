#!/usr/bin/env bash

echo "Creating in/output directories"

mkdir -p output/

mkdir -p input/

cd input

curl https://cdn.hobson.co.za/file/ComputerVision/Medical%2Bmask.zip > MedicalMask.zip && unzip -q MedicalMask.zip && rm MedicalMask.zip &
mkdir CMFD && cd CMFD && curl https://cdn.hobson.co.za/file/ComputerVision/CMFD.zip > cmfd.zip && unzip -q cmfd.zip && rm cmfd.zip &
mkdir IMFD && cd IMFD && curl https://cdn.hobson.co.za/file/ComputerVision/IMFD.zip > imfd.zip && unzip -q imfd.zip && rm imfd.zip &
mkdir CMFD1 && cd CMFD1 && curl https://cdn.hobson.co.za/file/ComputerVision/CMFD1.zip > cmfd1.zip && unzip -q cmfd1.zip && rm cmfd1.zip &
mkdir IMFD1 && cd IMFD1 && curl https://cdn.hobson.co.za/file/ComputerVision/IMFD1.zip > imfd1.zip && unzip -q imfd1.zip && rm imfd1.zip &

wait

echo "Data preperation complete"
