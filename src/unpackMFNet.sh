#!/bin/sh

cd input/

TARGET_CMDF="./images/face_with_mask/"
TARGET_IMDF="./images/face_no_mask/"
SOURCE_CMDF="./CMFD/"
SOURCE_IMDF="./IMFD/"
SOURCE_CMDF1="./CMFD1/"
SOURCE_IMDF1="./IMFD1/"

CMDFFILES=$(find $SOURCE_CMDF -type f)
IMDFFILES=$(find $SOURCE_IMDF -type f)
CMDFFILES1=$(find $SOURCE_CMDF1 -type f)
IMDFFILES1=$(find $SOURCE_IMDF1 -type f)

for f in $CMDFFILES; do
    mv $f $TARGET_CMDF
done

for f in $IMDFFILES; do
    mv $f $TARGET_IMDF
done

for f in $CMDFFILES1; do
    mv $f $TARGET_CMDF
done

for f in $IMDFFILES1; do
    mv $f $TARGET_IMDF
done
